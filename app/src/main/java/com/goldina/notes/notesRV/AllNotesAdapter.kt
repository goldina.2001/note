package com.goldina.notes.notesRV

import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.goldina.notes.R
import com.goldina.notes.data.Note
import kotlinx.android.synthetic.main.item.view.*

class AllNotesAdapter(val context: Context, val noteList: List<Note>)
    :RecyclerView.Adapter<AllNotesAdapter.AllNotesHolder>(){
    class AllNotesHolder(itemView:View):RecyclerView.ViewHolder(itemView) {
        var textViewString:TextView

        init {
            textViewString=itemView.textViewString
        }

    }
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AllNotesHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item,parent,false)
        return AllNotesHolder(view)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: AllNotesAdapter.AllNotesHolder, position: Int) {
        holder.textViewString.text = noteList[position].content
    }

    override fun getItemCount(): Int {
        return noteList.size
    }

    fun findNote(position: Int): Int {
        return noteList[position].id
    }


}