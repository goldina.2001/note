package com.goldina.notes

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.goldina.notes.data.ContactDatabase
import com.goldina.notes.data.ContactViewModel
import com.goldina.notes.notesRV.AllNotesAdapter
import kotlinx.android.synthetic.main.activity_note.*


class NoteActivity : AppCompatActivity() {
    private lateinit var myAdapter: AllNotesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_note)
        val contactDao = ContactDatabase.getDatabase(this).contactDao

        val name = intent.extras!!["contact_name"].toString()
        val id = intent.extras!!["contact_id"].toString().toInt()

        textView.text="Заметки для контакта $name"
        rvNote.layoutManager= LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false)
        var size = 0
        contactDao.getContactWithNotes(id).observe(this, Observer {
            if(it!=null) {
                size = it.size
                myAdapter = AllNotesAdapter(baseContext, it)
                myAdapter.notifyDataSetChanged()
                rvNote.adapter = myAdapter
            }else Toast.makeText(this, "Заметок нет", Toast.LENGTH_LONG).show()
        })

        floatingActionButton.setOnClickListener{
            val intent = Intent(this, EditNoteActivity::class.java)
            intent.putExtra("add", true)
            intent.putExtra("contact_id", id)
            startActivity(intent)
        }
        with(ItemClickSupport.addTo(rvNote)) {
            setOnItemClickListener { _, position, _ ->  openEditNote(this@NoteActivity, position)}
        }
    }

    private fun openEditNote(activity: Activity, position: Int) {
        val noteId = myAdapter.findNote(position)
        val intent = Intent(activity, EditNoteActivity::class.java)
        intent.putExtra("add", false)
        intent.putExtra("id", noteId)
        activity.startActivity(intent)
    }
}