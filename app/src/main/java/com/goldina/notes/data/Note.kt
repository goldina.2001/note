package com.goldina.notes.data

import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "note")
data class Note(
    @PrimaryKey(autoGenerate = false)
    val id:Int,
    val content:String,
    val idContact:Int
)