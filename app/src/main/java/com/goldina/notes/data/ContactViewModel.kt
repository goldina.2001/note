package com.goldina.notes.data

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ContactViewModel(application: Application): AndroidViewModel(application) {
    private val readAllData: LiveData<List<Contact>>
     private val repository: ContactRepository
    init {
        val contactDao = ContactDatabase.getDatabase(application).contactDao
        repository = ContactRepository(contactDao)
        readAllData = repository.readAllData
    }

     fun addContact(contact: Contact){
        viewModelScope.launch(Dispatchers.IO) {
            repository.addContact(contact)
        }
    }
    fun addNote(note: Note){
        viewModelScope.launch(Dispatchers.IO) {
            repository.addNote(note)
        }
    }

    fun updateNote(id:Int,content:String){
        viewModelScope.launch(Dispatchers.IO) {
            repository.updateNote(id,content)
        }
    }

    fun deleteNote(id:Int){
        viewModelScope.launch(Dispatchers.IO) {
            repository.deleteNote(id)
        }
    }
}