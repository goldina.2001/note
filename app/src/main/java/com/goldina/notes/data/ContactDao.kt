package com.goldina.notes.data
import androidx.lifecycle.LiveData
import androidx.room.*


@Dao
interface ContactDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addContact(contact: Contact)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addNote(note: Note)

    @Query("SELECT * FROM contacts ORDER BY id ASC")
    fun readAllContact(): LiveData<List<Contact>>

    @Query("SELECT * FROM note ORDER BY id ASC")
    fun readAllNote(): LiveData<List<Note>>

    @Transaction
    @Query("SELECT * FROM note WHERE idContact = :id")
    fun getContactWithNotes(id:Int):LiveData<List<Note>>

    @Query("SELECT * FROM note WHERE id = :id")
    fun findNote(id:Int):LiveData<List<Note>>

    @Query("UPDATE note SET content =:content WHERE id = :id")
    fun updateNote(id:Int,content:String)

    @Query("DELETE FROM note WHERE id = :id")
    fun deleteNote(id:Int)

}