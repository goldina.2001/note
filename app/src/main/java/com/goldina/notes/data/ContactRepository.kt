package com.goldina.notes.data

import androidx.lifecycle.LiveData

class ContactRepository(private val contactDao: ContactDao) {

    val readAllData: LiveData<List<Contact>> = contactDao.readAllContact()
    suspend fun addContact(contact: Contact){
        contactDao.addContact(contact)
    }
    suspend fun addNote(note: Note){
        contactDao.addNote(note)
    }

    suspend fun updateNote(id:Int,content:String){
        contactDao.updateNote(id,content)
    }

    suspend fun deleteNote(id:Int){
        contactDao.deleteNote(id)
    }


}