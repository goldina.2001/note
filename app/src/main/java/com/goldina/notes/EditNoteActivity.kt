package com.goldina.notes

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.goldina.notes.data.ContactDatabase
import com.goldina.notes.data.ContactViewModel
import com.goldina.notes.data.Note
import kotlinx.android.synthetic.main.activity_edit_note.*

class EditNoteActivity : AppCompatActivity() {
    private lateinit var mContactViewModel: ContactViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_note)
        val activity = this
        val checkAction = intent.extras!!["add"].toString().toBoolean()
        var id = 0
        var new_id = 1
        var idContact=0
        var content = ""
        mContactViewModel = ViewModelProvider(this).get(ContactViewModel::class.java)
        val contactDao = ContactDatabase.getDatabase(this).contactDao
        if (checkAction){
            idContact = intent.extras!!["contact_id"].toString().toInt()
            contactDao.readAllNote().observe(this, Observer {
                if(it.isNotEmpty()) new_id = it.last().id+1
            })

        }else {
           id = intent.extras!!["id"].toString().toInt()
            contactDao.findNote(id).observe(this, Observer {
                content = it.first().content
                idContact = it.first().idContact
                editTextTextMultiLine.setText(content)
            })
        }
        bottomNavigationView.setOnItemSelectedListener {
            when(it.itemId){
                R.id.save -> {
                    if(TextUtils.isEmpty(editTextTextMultiLine.text.toString()))
                        Toast.makeText(this, "Введите вашу заметку", Toast.LENGTH_SHORT).show()
                    else{
                        if (!checkAction){
                            mContactViewModel.updateNote(id,editTextTextMultiLine.text.toString())
                        }else{
                            val note = Note(new_id,editTextTextMultiLine.text.toString(),idContact)
                            mContactViewModel.addNote(note)
                        }
                        onBackPressed()
                    }
                }
                R.id.delete ->{
                    if (!checkAction){
                            mContactViewModel.deleteNote(id)
                    }
                    onBackPressed()
                }
            }
            true

        }

    }
}