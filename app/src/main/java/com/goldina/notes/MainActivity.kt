package com.goldina.notes

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.provider.ContactsContract
import android.util.Log
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.goldina.notes.contactRV.AllContact
import com.goldina.notes.contactRV.AllContactAdapter
import com.goldina.notes.data.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private val adapter = AllContactAdapter()
    private lateinit var mContactViewModel: ContactViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        rvContact.layoutManager= LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false)
        rvContact.adapter=adapter
        mContactViewModel =ViewModelProvider(this).get(ContactViewModel::class.java)
        getContacts()
        with(ItemClickSupport.addTo(rvContact)) {
            setOnItemClickListener { _, position, _ ->  openNote(this@MainActivity, position)}
        }

    }
    @SuppressLint("Range")
    fun getContacts(){

        val cursor = contentResolver.query(
            ContactsContract.Contacts.CONTENT_URI,
            null,
            null,
            null,
            null
        )

        var count = 1
        if (cursor != null && cursor.moveToFirst()) {
            do {
                val name =
                    cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME))
                adapter.addContact(AllContact(count,name))
                val contact = Contact( count,name)
                count++
                mContactViewModel.addContact(contact)
                Log.d("getContacts", "name:$name")
            } while (cursor.moveToNext())
            cursor.close()
        }
    }
    private fun openNote(activity: Activity, position: Int) {
        val contact = adapter.findContact(position)
        val intent = Intent(activity, NoteActivity::class.java)
        intent.putExtra("contact_id", contact.id)
        intent.putExtra("contact_name", contact.name)
        activity.startActivity(intent)
    }
}