package com.goldina.notes.contactRV

import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.goldina.notes.R
import com.goldina.notes.databinding.ItemBinding

class AllContactAdapter: RecyclerView.Adapter<AllContactAdapter.AllContactHolder>()  {
    private val contactList = ArrayList<AllContact>()
    class AllContactHolder(item: View):RecyclerView.ViewHolder(item) {
        private val binding = ItemBinding.bind(item)
        @RequiresApi(Build.VERSION_CODES.M)
        fun bind(allContact: AllContact)=with(binding){
            textViewString.text=allContact.name
        }

    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AllContactHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item,parent,false)
        return AllContactHolder(view)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: AllContactAdapter.AllContactHolder, position: Int) {
        holder.bind(contactList[position])
    }

    override fun getItemCount(): Int {
        return contactList.size
    }

    fun addContact(allContact: AllContact){
        contactList.add(allContact)
        notifyDataSetChanged()
    }

    fun findContact(position: Int):AllContact{
        return contactList[position]
    }
}